﻿using GTA;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace LapTimerStopwatch
{
    public class LapTimerStopwatch_Script : Script
    {
        bool stopWatchRunning = false, backgroundEnabled, selectedStartNumLaps = false;
        List<Stopwatch> stopwatches = new List<Stopwatch>();
        float backgroundPosX, backgroundPosY, textPosX, textPosY, backgroundWidth, backgroundHeight;
        Keys scriptToggleHoldKey, scriptTogglePressKey, stopwatchStartNextKey, stopwatchResetHoldKey, numLapsAdd, numLapsSub, selectNumLaps;
        int backgroundColor, textColor, startNumLaps = 2, currentLap = 0;

        public LapTimerStopwatch_Script()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("BACKGROUND_POS_X", "POSITIONS", 40);
                Settings.SetValue("BACKGROUND_POS_Y", "POSITIONS", 40);
                Settings.SetValue("TEXT_POS_X", "POSITIONS", 45);
                Settings.SetValue("TEXT_POS_Y", "POSITIONS", 41);
                Settings.SetValue("BACKGROUND_WIDTH", "SIZES", 240);
                Settings.SetValue("BACKGROUND_HEIGHT", "SIZES", 30);
                Settings.SetValue("SCRIPT_TOGGLE_HOLD_KEY", "KEYS", Keys.RControlKey);
                Settings.SetValue("SCRIPT_TOGGLE_PRESS_KEY", "KEYS", Keys.S);
                Settings.SetValue("STOPWATCH_START_NEXT_KEY", "KEYS", Keys.OemPeriod);
                Settings.SetValue("STOPWATCH_RESET_HOLD_KEY", "KEYS", Keys.Oemcomma);
                Settings.SetValue("BACKGROUND_ENABLED", "ENABLED", true);
                Settings.SetValue("BACKGROUND_COLOR", "COLORS", Color.Black.ToArgb());
                Settings.SetValue("TEXT_COLOR", "COLORS", Color.White.ToArgb());
                Settings.SetValue("NUM_LAPS_ADD_KEY", "KEYS", Keys.NumPad6);
                Settings.SetValue("NUM_LAPS_SUB_KEY", "KEYS", Keys.NumPad4);
                Settings.SetValue("SELECT_NUM_LAPS_KEY", "KEYS", Keys.Enter);
                Settings.Save();
            }

            backgroundPosX = Settings.GetValueFloat("BACKGROUND_POS_X", "POSITIONS", 40);
            backgroundPosY = Settings.GetValueFloat("BACKGROUND_POS_Y", "POSITIONS", 40);
            textPosX = Settings.GetValueFloat("TEXT_POS_X", "POSITIONS", 45);
            textPosY = Settings.GetValueFloat("TEXT_POS_Y", "POSITIONS", 41);
            backgroundWidth = Settings.GetValueFloat("BACKGROUND_WIDTH", "SIZES", 240);
            backgroundHeight = Settings.GetValueFloat("BACKGROUND_HEIGHT", "SIZES", 30);
            scriptToggleHoldKey = Settings.GetValueKey("SCRIPT_TOGGLE_HOLD_KEY", "KEYS", Keys.RControlKey);
            scriptTogglePressKey = Settings.GetValueKey("SCRIPT_TOGGLE_PRESS_KEY", "KEYS", Keys.S);
            stopwatchStartNextKey = Settings.GetValueKey("STOPWATCH_START_NEXT_KEY", "KEYS", Keys.OemPeriod);
            stopwatchResetHoldKey = Settings.GetValueKey("STOPWATCH_RESET_HOLD_KEY", "KEYS", Keys.Oemcomma);
            backgroundEnabled = Settings.GetValueBool("BACKGROUND_ENABLED", "ENABLED", true);
            backgroundColor = Settings.GetValueInteger("BACKGROUND_COLOR", "COLORS", Color.Black.ToArgb());
            textColor = Settings.GetValueInteger("TEXT_COLOR", "COLORS", Color.White.ToArgb());
            numLapsAdd = Settings.GetValueKey("NUM_LAPS_ADD_KEY", "KEYS", Keys.NumPad6);
            numLapsSub = Settings.GetValueKey("NUM_LAPS_SUB_KEY", "KEYS", Keys.NumPad4);
            selectNumLaps = Settings.GetValueKey("SELECT_NUM_LAPS_KEY", "KEYS", Keys.Enter);

            while (stopwatches.ToArray().Length < 6) stopwatches.Add(new Stopwatch());

            KeyDown += StopWatch_Script_KeyDown;
            PerFrameDrawing += StopWatch_Script_PerFrameDrawing;
        }

        public void StopWatch_Script_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(scriptToggleHoldKey) && e.Key == scriptTogglePressKey)
            {
                stopWatchRunning = !stopWatchRunning;
                Subtitle((stopWatchRunning ? "Stopwatch enabled" : "Stopwatch disabled"));
            }

            if (stopWatchRunning)
            {
                if (selectedStartNumLaps)
                {
                    if (!isKeyPressed(stopwatchResetHoldKey) && e.Key == stopwatchStartNextKey)
                    {
                        if (currentLap == 0)
                        {
                            if (stopwatches[currentLap].IsRunning)
                            {
                                if (!stopwatches[currentLap].Equals(null)) stopwatches[currentLap].Stop();
                                currentLap++;
                                stopwatches[currentLap].Start();
                            }
                            else
                            {
                                stopwatches[currentLap].Start();
                            }
                        }
                        else if (currentLap < (startNumLaps - 1))
                        {
                            if (!stopwatches[currentLap].Equals(null) && stopwatches[currentLap].IsRunning) stopwatches[currentLap].Stop();
                            currentLap++;
                            stopwatches[currentLap].Start();
                        }
                        else
                        {
                            Subtitle("Race finished!");
                            foreach (Stopwatch s in stopwatches) if (s.ElapsedMilliseconds != 0 || s.IsRunning) s.Stop();
                        }
                    }
                    if (isKeyPressed(stopwatchResetHoldKey) && e.Key == stopwatchStartNextKey)
                    {
                        selectedStartNumLaps = false;
                        foreach (Stopwatch s in stopwatches) if (s.IsRunning || s.ElapsedMilliseconds != 0) s.Reset();
                        currentLap = 0;
                    }
                }
                else
                {
                    if (e.Key == numLapsAdd)
                    {
                        if (startNumLaps < 5)
                        {
                            startNumLaps++;
                        }
                        else
                        {
                            startNumLaps = 1;
                        }
                    }
                    else if (e.Key == numLapsSub)
                    {
                        if (startNumLaps > 1)
                        {
                            startNumLaps--;
                        }
                        else
                        {
                            startNumLaps = 5;
                        }
                    }
                    if (e.Key == selectNumLaps)
                    {
                        selectedStartNumLaps = true;
                    }
                }
            }

            /*if (e.Key == Keys.D9)
            {
                backgroundWidth++;
                Subtitle(backgroundWidth.ToString());
            }
            if (e.Key == Keys.D8)
            {
                backgroundWidth--;
                Subtitle(backgroundWidth.ToString());
            }*/
        }

        public void StopWatch_Script_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (stopWatchRunning)
            {
                if (selectedStartNumLaps)
                {
                    for (int i = 0; i <= currentLap; i++)
                    {
                        string endUserString = ("Lap " + (i + 1).ToString() + ": ") + string.Format("{0}:{1:00}:{2:00}:{3:00}.{4:000}", stopwatches[i].Elapsed.Days, stopwatches[i].Elapsed.Hours, stopwatches[i].Elapsed.Minutes, stopwatches[i].Elapsed.Seconds, stopwatches[i].Elapsed.Milliseconds);
                        if (backgroundEnabled)
                        {
                            e.Graphics.DrawRectangle(new System.Drawing.RectangleF(backgroundPosX, backgroundPosY + (i * backgroundHeight), backgroundWidth, backgroundHeight), Color.FromArgb(backgroundColor));
                        }

                        e.Graphics.DrawText(endUserString, textPosX, textPosY + (i * backgroundHeight), Color.FromArgb(textColor));
                    }
                }
                else
                {
                    e.Graphics.DrawText("Number of laps: " + startNumLaps.ToString(), textPosX, textPosY, Color.FromArgb(textColor));
                }
            }
            else
            {
                foreach (Stopwatch s in stopwatches) if (s.IsRunning || s.ElapsedMilliseconds != 0) s.Reset();
                currentLap = 0;
                startNumLaps = 2;
                selectedStartNumLaps = false;
            }
        }

        public void Subtitle(string info, int millisecs = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", info, millisecs, 1);
        }
    }
}
