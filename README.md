About mod:
The name pretty much says it all.

How to use it:
Press RCTRL + S to toggle stopwatch.
Press NUMPAD8 to increase the number of laps.
Press NUMPAD4 to decrease the number of laps.
Press ENTER to select the number of laps.
Press the period button to start the race/go to next lap
Press comma + period together (press comma first) to reset all laps.

What you can change in the ini file:
Position of where the text goes.
Position of where the background goes.
Size of the backgroud.
Color of the text.
Color of the background.
Whether to show the background or not.
The keys.

Also:
The ini file is created the first time the script is ran.
To change the colors you will need to get the ARGB integer for that color. Use the program 'ColorFinder.exe' in the zip file to help with that.
The maximum amount of laps is 5, at the moment.

Thanks to:
Aru: C++ Scripthook.
HazardX: .NET Scripthook.
Adadxsg/twattyballs2011: The request(s).

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
